# Laravel Minecraft CMS

A simple minecraft cms. Based on Laravel 5.6

## Requirements

- Laravel 5.6
- PHP >= 7.1.3
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

## Installation

```
git clone https://gitlab.com/FoxSeaSha/Laravel-CMS-Minecraft.git namedir.loc
composer install
npm install
php artisan key:generate
php artisan migrate
```

## API

//

```
//

```
